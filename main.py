import crochet

crochet.setup()
import pandas as pd
from flask import Flask, render_template, jsonify, request, redirect, url_for
from scrapy import signals
from scrapy.crawler import CrawlerRunner
from scrapy.signalmanager import dispatcher
import time
import nltk
from sklearn import feature_extraction
import os
import sys
from auto_tqdm import tqdm
import sqlite3 as sql  
from flask_bootstrap import Bootstrap
from textblob import TextBlob 
from ScrapeAmazon.ScrapeAmazon.spiders.amazon_scraping import ReviewspiderSpider
import re
import string
import pandas as pd
import numpy as np
from flask import json
import price
import product_imagelink
import product_description

app = Flask(__name__)
Bootstrap(app)

output_data = []
crawl_runner = CrawlerRunner()
df=pd.read_excel('Product.xlsx')


@app.route('/')
def index():
    df=pd.read_excel('Product.xlsx')
    colours=[]
    colours=df["Product"]
    print(colours)
    return render_template("index.html",colours=colours)

@app.route('/home')
def home():
    return render_template("home.html")

@app.route('/contact')
def contact():
    return render_template("contact.html")

@app.route('/about')
def about():
    return render_template("about.html")

@app.route('/link')
def link():
    return render_template("link.html")



@app.route('/', methods=['POST'])
def submit():
    if request.method == 'POST':
        df=pd.read_excel('Product.xlsx')
        p = request.form['colours']  
        
        df1=df.loc[(df['Product'] == p)]
        s=df1['Link'].tolist()
        print(s[0])
        global baseURL
        baseURL = s[0]
        global protitle
        pnmae=str(s).split('//')
        protitle=pnmae[1].split('/')
        protitle=protitle[1]

        
        if os.path.exists("ScrapeAmazon/outputfile.json"):
            print('path')
            try:
                os.remove("ScrapeAmazon/outputfile.json")
            except OSError as e: 
                print ("Failed with:", e.strerror) 
                print ("Error code:", e.code)
        return redirect(url_for('scrape'))  


@app.route("/scrape")
def scrape():
    #global baseURL
    #global output_data
    #table_name = baseURL.split("?")[0]
    table_name = "product"
    conn = sql.connect('database.db')
    global prod_price
    global prod_image
    global prod_desc
    c = conn.cursor()
    c.execute('''SELECT count(name) FROM sqlite_master WHERE name='%s' AND type='table' ''' % table_name)

    if (c.fetchone()[0] == 0):
        output_data.clear()
        print(baseURL)
        scrape_with_crochet(baseURL=baseURL)
        time.sleep(10)

        conn.execute(
            '''CREATE TABLE '%s' (names TEXT,  reviewerLink TEXT, reviewTitles TEXT, reviewBody TEXT, verifiedPurchase TEXT, postDate TEXT, starRating TEXT, helpful TEXT, nextPage TEXT, sentiment TEXT)''' % table_name)

        for x in output_data:
            if (protitle == x['product_title']):
                processed_reviews =  process(x["reviewBody"])
                sentiment = get_tweet_sentiment(processed_reviews[0])

                c.execute(
                    '''INSERT INTO '%s' (names, reviewerLink, reviewTitles, reviewBody, verifiedPurchase, postDate, starRating, helpful, nextPage, sentiment) VALUES (?,?,?,?,?,?,?,?,?,?)''' % table_name,
                    (
                    x["names"], x["reviewerLink"], x["reviewTitles"], x["reviewBody"], x["verifiedPurchase"], x["postDate"],
                    x["starRating"], x["helpful"], x["nextPage"], sentiment))

        conn.commit()
        conn.close()
        priceurl=baseURL
        priceurl=priceurl.rsplit('/', 1)[-2]
        print(priceurl)
        prod_price = price.price_of_product(priceurl)
        prod_image=product_imagelink.image_link_of_product(priceurl)
        prod_desc=product_description.get_product_desc(priceurl)

        print("Table and Records created Successfully!")


    else:  
        output_data.clear()
        
        scrape_with_crochet(baseURL=baseURL)
        time.sleep(10)

        conn.execute(
            '''Delete from  '%s' ''' % table_name)
        conn.commit()

        for x in output_data:
            if (protitle == x['product_title']):
                processed_reviews =  process(x["reviewBody"])
                sentiment = get_tweet_sentiment(processed_reviews[0])

                c.execute(
                    '''INSERT INTO '%s' (names, reviewerLink, reviewTitles, reviewBody, verifiedPurchase, postDate, starRating, helpful, nextPage, sentiment) VALUES (?,?,?,?,?,?,?,?,?,?)''' % table_name,
                    (
                    x["names"], x["reviewerLink"], x["reviewTitles"], x["reviewBody"], x["verifiedPurchase"], x["postDate"],
                    x["starRating"], x["helpful"], x["nextPage"], sentiment))

        conn.commit()
        conn.close()

        priceurl=baseURL
        priceurl=priceurl.rsplit('/', 1)[-2]
        print(priceurl)
        
        prod_price=price.price_of_product(priceurl)
        prod_image =product_imagelink.image_link_of_product(priceurl)
        prod_desc=product_description.get_product_desc(priceurl)

        print("Table and Records created Successfully!")
        return redirect(url_for('reviews'))  

    # return jsonify(output_data)

@app.route('/reviews')
def reviews():
    price_product=prod_price
    image_product=prod_image
    desc_product=prod_desc
    conn = sql.connect('database.db')
    table_name = "product"
    c = conn.cursor()
    conn.row_factory = sql.Row
    cur = conn.cursor()
    cur.execute(''' SELECT * from '%s' ''' % table_name)
    rows = cur.fetchall()
    return render_template('reviews.html', data=rows,message=price_product,image=image_product,descriptions=desc_product)

@app.route('/add_link',methods=['POST'])   
def add_link():

    name = request.form['name']
    url = request.form['url']
    output=[]
    output.append(name)
    output.append(url)
    df=pd.read_excel('Product.xlsx',index_col=0)
    output_data=pd.DataFrame([output],columns = ['Product','Link'])
    df3=df.append(output_data)
    df3.reset_index()
    #df3 = df.loc[:, ~df3.columns.str.contains('^Unnamed')]
    df3.to_excel('Product.xlsx')
    df=pd.read_excel('Product.xlsx')
    colours=[]
    colours=df["Product"]
    print(colours)


    return render_template('index.html',colours=colours)


@app.route('/graph',methods=['POST'])   
def graph():

    conn = sql.connect('database.db')
    table_name = "product"
    c = conn.cursor()
    conn.row_factory = sql.Row
    cur = conn.cursor()
    cur.execute('''SELECT COUNT( sentiment) as count FROM  '%s' GROUP BY sentiment ''' % table_name)
    data1 = cur.fetchall()
    val=[]
    if len(data1) > 2:
        for row in data1 :
            val.append(row[0])
    else:
        for row in data1 :
            val.append(row[0])
    print(val)



    return render_template('graph.html', val=json.dumps(val))


@crochet.run_in_reactor
def scrape_with_crochet(baseURL):

    dispatcher.connect(_crawler_result, signal=signals.item_scraped)
    eventual = crawl_runner.crawl(ReviewspiderSpider, category=baseURL)



def _crawler_result(item, response, spider):
    
    output_data.append(dict(item))


_wnl = nltk.WordNetLemmatizer()

def normalize_word(w):
    return _wnl.lemmatize(w).lower()

def get_tokenized_lemmas(s):
    return [normalize_word(t) for t in nltk.word_tokenize(s)]

def clean(text):
    # Cleans a string: Lowercasing, trimming, removing non-alphanumeric
    text = text.translate(str.maketrans(string.punctuation, ' '*len(string.punctuation),''))
    return " ".join(re.findall(r'\w+', text, flags=re.UNICODE)).lower()

def remove_stopwords(l):
    # Removes stopwords from a list of tokens
    return [w for w in l if w not in feature_extraction.text.ENGLISH_STOP_WORDS]
def join_tok(text):
    return " ".join(text).lower()

def process(texts):
    lst=[]
    clean_text= clean(texts)
    tok_text= get_tokenized_lemmas(clean_text)
    remov_stp= remove_stopwords(tok_text)
    lst.append(join_tok(remov_stp))
    return lst

def get_tweet_sentiment(tweet): 

    analysis = TextBlob(tweet) 
    
    if analysis.sentiment.polarity > 0: 
            return 'positive'
    elif analysis.sentiment.polarity == 0: 
            return 'neutral'
    else: 
            return 'negative'

if __name__ == "__main__":
    app.run(debug=True)
