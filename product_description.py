import requests
from glob import glob
from bs4 import BeautifulSoup
import pandas as pd
from datetime import datetime
from time import sleep


def get_product_desc(url1):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}

    #url = 'https://www.amazon.in/Apple-iPhone-11-Pro-256GB/dp/B07XVMJF2D'
    url=url1
    response = requests.get(url, headers=headers)
    #print(response.text)

    soup = BeautifulSoup(response.content, features="lxml")

    #title = soup.select("#productTitle")[0].get_text().strip()
    # price_ = soup.find(id="priceblock_ourprice").get_text()[1:].strip().replace(',','')
    # get_product_description=soup.find("div", {"id": "imgTagWrapperId"}).find("img")

    # get_uls=soup.find_all('ul', class_='a-unordered-list a-vertical a-spacing-mini')

    product_description_list=[]
    for ultag in soup.find_all('ul', {'class':'a-unordered-list a-vertical a-spacing-mini'}):
        for li in ultag.find_all('li'):
            product_description_list.append(li.text)
            print(li.text)

    if(len(product_description_list)!=0):
        description_list=product_description_list
    else:
        description_list="Not Found"
    return(description_list)

