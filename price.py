import requests
from glob import glob
from bs4 import BeautifulSoup
import pandas as pd
from datetime import datetime
from time import sleep


def price_of_product(url1):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}

    #url = 'https://www.amazon.in/Apple-iPhone-11-Pro-256GB/dp/B07XVMJF2D'
    url=url1
    response = requests.get(url, headers=headers)
    #print(response.text)

    soup = BeautifulSoup(response.content, features="lxml")

    #title = soup.select("#productTitle")[0].get_text().strip()
    price_ = soup.find(id="priceblock_ourprice").get_text()[1:].strip().replace(',','')
    if(price_ != ""):
        price1=price_
    else:
        price1="Not Found"
    return(price1)

